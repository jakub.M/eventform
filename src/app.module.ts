import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EventModule } from './event/event.module';

@Module({
    imports: [TypeOrmModule.forRoot(), EventModule],
    controllers: [],
    providers: [],
  })
  export class AppModule {}