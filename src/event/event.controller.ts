import { Controller, Post, Body } from '@nestjs/common';
import { Settings } from '../Constants/Settings';
import { EventService } from './event.service';
import { Event } from '../entities/event.entity';
import { ApiUseTags, ApiCreatedResponse, ApiOperation, ApiBadRequestResponse } from '@nestjs/swagger';
import { eventDto } from '../dtos/event.dto';

@ApiUseTags('event')
@Controller(`${Settings.apiUrl}/event`)
export class EventController {
    constructor(private readonly eventService: EventService){}

    @ApiOperation({title: 'Crate new event'})
    @ApiCreatedResponse({description: 'The record has been successful created'})
    @ApiBadRequestResponse({description: 'Validation error'})
    @Post()
    async create(@Body() req: eventDto): Promise<Event>{
        console.log(req)
        return await this.eventService.Insert(req);
    }
}
