import { Injectable } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";
import { Event } from '../entities/event.entity';
import { Repository } from 'typeorm';
import { eventDto } from '../dtos/event.dto';


@Injectable()
export class EventService {
    constructor(@InjectRepository(Event) private readonly eventRepository: Repository<Event>){}
    
    async Insert(req: eventDto):Promise<Event> {
        const event = new Event(
            req.firstName,
            req.lastName,
            req.email,
            req.date
        )
        await this.eventRepository.save(event);
        return event;
    }
}
