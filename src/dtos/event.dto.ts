import { IsNotEmpty, IsEmail, IsDateString, IsString, IsDate } from "class-validator";
import { ApiModelProperty } from "@nestjs/swagger";
export class eventDto{
    @ApiModelProperty({
        example: 'John'
    })
    @IsNotEmpty()   
    @IsString() 
    firstName: String;
    
    @ApiModelProperty({
        example: 'Smith'
    })
    @IsNotEmpty()
    lastName: String;
    
    @ApiModelProperty({
        example: 'example@example.com'
    })
    @IsEmail()
    email: String;
    
    @ApiModelProperty({
        example: '2017-06-07T14:34:08.700'
    })
    @IsNotEmpty()
    @IsDateString()
    date: Date;
}