import { Entity, ObjectIdColumn, ObjectID, Column } from "typeorm";
import { ApiModelProperty } from "@nestjs/swagger";

@Entity()
export class Event{
    @ApiModelProperty()
    @ObjectIdColumn() id: ObjectID;

    @ApiModelProperty({
        example: 'John'
    })
    @Column() firstName: String;

    @ApiModelProperty({
        example: 'Smith'
    })
    @Column() lastName: String;

    @ApiModelProperty({
        example: 'example@example.com'
    })
    @Column() email: String;

    @ApiModelProperty({
        example: '18.02.2019'
    })
    @Column() date: Date;

    constructor(firstName: String, lastName: String, email: String, date: Date) {    
        this.firstName = firstName;
        this.lastName = lastName;    
        this.email = email;
        this.date = date;
    }
}