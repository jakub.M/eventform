import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";
import { ValidationPipe } from '@nestjs/common';
import * as cors from "cors";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const options = new DocumentBuilder()
  .setTitle('Events')
  .setDescription('The events api')
  .setVersion('1.0')
  .addTag('event')
  .build()

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('swagger',app,document);

  app.useGlobalPipes(new ValidationPipe());
  app.use(cors());
  await app.listen(3100);
}
bootstrap();
