
## 1. Setup

```bash
$ git clone https://gitlab.com/jakub.M/eventform.git

$ cd eventform
$ npm i

$ cd client
$ npm i
```

## 2. Run

```bash
# back-end
$ npm run server

# front-end
$ npm run client

# both
$ npm run dev
```
Frontend
https://localhost:3000

Backend
https://localhost:3100

Backend documetntation
https://localhost:3100/swagger



## 3. Test

```bash
# front-end
$ npm run client:test

# back-end
$ npm run test
```