import { ADD_EVENT_SUCCESS, ADD_EVENT_FAILURE, ADD_EVENT_STARTED } from "./types";
import axios from 'axios';
import { endpoint } from '../constants/api';

export const addEvent = event =>{
    return dispatch => {
        dispatch(addEventStarte());
        console.log(event)
        axios
            .post(`${endpoint}/event`, event)
            .then(res => dispatch(addEventSucces()))
            .catch(err => {
                return dispatch(addEventFailure(err.message))
            })
    }
}

export const addEventStarte = () => ({
    type: ADD_EVENT_STARTED
})

export const addEventSucces = () =>({
    type: ADD_EVENT_SUCCESS
})

export const addEventFailure = err => ({
    type: ADD_EVENT_FAILURE,
    payload: {err},
})