import reducer from '../reducers/eventReducer'
import * as types from '../actions/types'

describe('event reducer', () => {
    it('should return initial state',()=>{
        expect(reducer(undefined,{})).toEqual(
            {
                loading: false,
                success: false,
                err: null
            })
    })

    it('should handle ADD_EVENT_STARTED',() =>{
        expect(
            reducer({},{
                type: types.ADD_EVENT_STARTED
            })
        ).toEqual({
            loading: true
        })
    })

    it('should handle ADD_EVENT_SUCCESS',() =>{
        expect(
            reducer({},{
                type: types.ADD_EVENT_SUCCESS
            })
        ).toEqual({
            loading: false,
            success:true,
            err: null
        })
    })

    it('should handle ADD_EVENT_FAILURE',() =>{
        expect(
            reducer({},{
                type: types.ADD_EVENT_FAILURE,
                payload: {err:"Request failed with status code 400"}
            })
        ).toEqual({
            success: false,
            loading: false,
            err: "Request failed with status code 400"
        })
    })
})