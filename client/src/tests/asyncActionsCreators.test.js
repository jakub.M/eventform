import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as actions from '../actions/index'
import * as types from '../actions/types'
import fetchMock from 'fetch-mock'
import expect from 'expect'
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('async actions', () => {
    afterEach(()=> {
        fetchMock.restore()
    })

    it('creates ADD_EVENT_SUCCES when posting event has been done',()=>{

        let mock = new MockAdapter(axios);

        mock.onPost().reply(200)

        const expectedActions =[
            {type: types.ADD_EVENT_STARTED},
            {type: types.ADD_EVENT_SUCCESS}
        ]

        const store = mockStore();

        return store.dispatch(actions.addEvent({})).then(()=>{
            expect(store.getActions()).toEqual(expectedActions)
        })
    })

    it('creates ADD_EVENT_FAILUER when posting event has not been done',()=>{

        let mock = new MockAdapter(axios);

        mock.onPost().reply(400)

        const expectedActions =[
            {type: types.ADD_EVENT_STARTED},
            {type: types.ADD_EVENT_FAILURE, payload: {err: "Request failed with status code 400"}}
        ]

        const store = mockStore();

        return store.dispatch(actions.addEvent({})).then(()=>{
            expect(store.getActions()).toEqual(expectedActions)
        })
    })
})