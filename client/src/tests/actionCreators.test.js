import * as actions from "../actions/index";
import * as types from "../actions/types";

describe('actions', () => {
    it('should create action to started add event', () => {
        const exeptedAction = {
            type: types.ADD_EVENT_STARTED
        }
        expect(actions.addEventStarted()).toEqual(exeptedAction)
    })

    it('should create action to success add event', () => {
        const exeptedAction = {
            type: types.ADD_EVENT_SUCCESS
        }
        expect(actions.addEventSucces()).toEqual(exeptedAction)
    })

    it('should create action to failure add event', () => {
        const payload = {}
        const exeptedAction = {
            type: types.ADD_EVENT_FAILURE,
            payload
        }
        expect(actions.addEventFailure()).toEqual(exeptedAction)
    })
})