import { combineReducers } from 'redux';
import eventReducer from "./eventReducer";
import { reducer as reduxFormReducer } from 'redux-form';

const reducers = combineReducers({
    eventReducer,
    form: reduxFormReducer
})

export default reducers;