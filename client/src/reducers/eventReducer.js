import { ADD_EVENT_STARTED, ADD_EVENT_SUCCESS, ADD_EVENT_FAILURE } from '../actions/types';

const initialState = {
    loading: false,
    success: false,
    err: null
}

const eventReducer = (state = initialState, {type, payload}) => { 
    switch(type){
        case ADD_EVENT_STARTED:
        return{
            ...state, 
            loading: true
        }
        case ADD_EVENT_SUCCESS:
        return{
            ...state, 
            loading: false,
            success: true,
            err: null,
        }
        case ADD_EVENT_FAILURE:
        return{
            ...state,
            loading: false,
            success: false,
            err: payload.err,
        }
        default:
         return state
    }
}

export default eventReducer;