import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import EventFormContainer from "../components/form/EventFormContainer";
import { Container, Card} from 'react-bootstrap';
class App extends Component {
  render() {
    return (
      <Container>
      <Card> 
          <Card.Header>Event Form App</Card.Header>
        <Card.Body>
            <BrowserRouter>
              <Switch>
                <Route exact path="/" component={EventFormContainer}/>
              </Switch>
            </BrowserRouter>
        </Card.Body>
      </Card>
      </Container>
    );
  }
}

export default App;
