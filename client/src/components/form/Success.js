import React from "react";

export const Success = () =>{
    return(
        <div className="text-center response">
            <h1>Thank You!</h1>
            <span>The form was submitted successfully.</span>
        </div>
    )
}