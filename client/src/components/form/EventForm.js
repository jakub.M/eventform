import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Input } from "../container/Input";
import { validate } from "./Validate";
import { Form} from 'react-bootstrap';
import { InputDatePicker } from "../container/InputDatePiecker";
class EventForm extends Component {
  render() {
      const {handleSubmit} = this.props
    return (            
            <Form onSubmit={handleSubmit}>
                <Field
                    name="firstName"
                    component={Input}
                    type="text"
                    label="First Name"
                />
                <Field
                    name="lastName"
                    component={Input}
                    type="text"
                    label="Last Name"
                />
                <Field
                    name="email"
                    component={Input}
                    type="email"
                    label="Email"
                />
                <Field
                    name="date"
                    component={InputDatePicker}
                    showTime={false}
                    type="text"
                    label="Date"
                    className="form-contorl"
                />
                <button
                type="submit" 
                className="submit-button"
                >Submit</button>
            </Form>
    );
  }
}

export default reduxForm({
    form: 'EventForm',
    validate
  })(EventForm);