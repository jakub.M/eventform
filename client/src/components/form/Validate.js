export const validate = values => {
    const errors = {}
    if (!values.firstName) {
      errors.firstName = 'First name is required'
    } else if (values.firstName.length > 20) {
      errors.firstName = 'Your Name is too long'
    }
    if (!values.lastName) {
        errors.lastName = 'Last name is required'
      } 
    if (!values.email) {
      errors.email = 'Email is required'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
      errors.email = 'Invalid email address'
    }
    if (!values.date) {
      errors.date = 'Date is required'
    }
    return errors
  }