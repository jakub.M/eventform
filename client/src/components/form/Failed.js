import React from "react";

export const Failed = () =>{
    return(
        <div className="text-center response">
            <h1>Something Wrong:(</h1>
            <span>There is error in sending! Please try again later.</span>
        </div>
    )
}