import React, { Component } from 'react';
import EventForm from "./EventForm";
import { addEvent } from '../../actions';
import { connect } from 'react-redux';
import { Failed } from "./Failed";
import { Success } from "./Success";

class EventFormContainer extends Component {
  render() {
    const {addEvent, success, err} = this.props;
    return (
      <div>
        {err !== null && <Failed />}
        {success === true && <Success/>}
        {(err === null && success === false) && <EventForm onSubmit={addEvent}/>}
      </div>
    );
  }
}

const mapStateToProps = state => {
  const {success, err} = state.eventReducer;
  return{
    err,
    success,
  }
}

const mapDispatchToProps = dispatch => {
  return{
    addEvent: event => dispatch(addEvent(event))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EventFormContainer);
