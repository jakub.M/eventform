import React from 'react';
import { Col, Form } from 'react-bootstrap';
import DatePicker from 'react-date-picker';


export const InputDatePicker  = ({ input, label, meta: { touched, error} , showTime }) =>
            <Col>
            <Form.Group>
              <Form.Label>{label}</Form.Label>
            <Form.Control hidden isInvalid={touched && error}/>
                <DatePicker
                    className={((touched && error) ? 'is-invalid form-control form-control-lg ':'form-control form-control-lg ')}
                    onChange={input.onChange}
                    format="DD MMM YYYY"
                    time={showTime}
                    value={!input.value ? null : new Date(input.value)}
                    isInvalid={touched && error}
                />
              {touched && (error && <div className="invalid-feedback">{error}</div>)}
            </Form.Group>
          </Col>
  