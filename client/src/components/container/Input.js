import React from 'react';
import { Col, Form, } from 'react-bootstrap';

export const Input = ({ input, label, type, meta: { touched, error} }) => (
      <Col>
            <Form.Group>
              <Form.Label>{label}</Form.Label>
              <Form.Control size="lg"
                {...input} placeholder={label} type={type} isInvalid={touched && error}
              />
              <Form.Control.Feedback type="invalid">
                {error}
              </Form.Control.Feedback>
            </Form.Group>
          </Col>
  )